package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;
import java.util.List;

import api.IManejadorPeliculas;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;
	
	private ILista<VOAgnoPelicula> peliculasAgno;

	
	public static final String SEPARADOR =",";

	public static final String SEPARADOR2 = "[|]";

	public static final String PARENTESIS1 = "[(]";

	public static final String PARENTESIS2 = "[)]";
	
	private int[] numbers;
    private int[] helper;

    private int number;
	


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		misPeliculas = new ListaEncadenada<VOPelicula>(); 
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();

		try{
			BufferedReader br = new BufferedReader(new FileReader(archivoPeliculas));

			String line = br.readLine();

			//Creamos un objeto temporal para llenar el arreglo misPeliculas
			VOPelicula temp = new VOPelicula();

			//Se crea un objeto temporal para llenar el arreglo de peliculasAgno
			VOAgnoPelicula tempAgno = new VOAgnoPelicula();

			while(line != null){

				//Partimos la linea que se acaba de leer
				String [] partes = line.split(SEPARADOR);


				//Se crea la lista para meter los generos y asignarlos al objeto VOPelicula
				ILista<String> generos = new ListaEncadenada<String>();

				if(partes.length == 3){

					String titulo =partes[1].split(PARENTESIS1)[0];
					temp.setTitulo(titulo.split("\"")[0]);

					String [] generosAsociados = partes[2].split(SEPARADOR2);
					for(int i =0; i < generosAsociados.length; i++){
						generos.agregarElementoFinal(generosAsociados[i]);
					}
					temp.setGenerosAsociados(generos);


					int agno =0;
					try{
						String[] arregloA�os = partes[1].split(PARENTESIS1);
						agno  = Integer.parseInt(arregloA�os[arregloA�os.length-1].split(PARENTESIS2)[0]);
					}
					catch(Exception e)
					{		

					}
					finally{
						temp.setAgnoPublicacion(agno);
						tempAgno.setAgno(agno);
					}
				}
				//Este if es para las lineas que tienen tres comas
				else if(partes.length == 4){


					//Se env�a el nombre de la pelicula
					String titulo = partes[1] + partes[2];
					temp.setTitulo(titulo.split("\"")[0]);

					//Se separan los generos asociados
					String [] generosAsociados = partes[3].split(SEPARADOR2 );


					// Se llena la lista de generos
					for(int i =0; i < generosAsociados.length; i++){
						generos.agregarElementoFinal(generosAsociados[i]);
					}
					// Se asigna la lista de generos a el objeto VOPelicula
					temp.setGenerosAsociados(generos);

					//Se asigna el a�o de publicaci�n
					int agno =0;
					try{
						String[] arregloA�os = partes[2].split(PARENTESIS1);

						agno = Integer.parseInt(arregloA�os[arregloA�os.length-1].split(PARENTESIS2)[0]);
					}
					catch(Exception e)
					{		

					}
					finally{

						temp.setAgnoPublicacion(agno);
						tempAgno.setAgno(agno);
					}

				}

				// Este if es para las lineas que cuatro comas
				else if(partes.length == 5){

					String titulo = partes[1]+partes[2]+partes[3];
					temp.setTitulo(titulo.split("\"")[0]);



					String[] generosAsociados = partes[4].split(SEPARADOR2);
					for(int i =0; i < generosAsociados.length; i++){
						generos.agregarElementoFinal(generosAsociados[i]);
					}

					temp.setGenerosAsociados(generos);

					//Se asigna el a�o de publicaci�n
					int agno =0;
					try{
						String[] arregloA�os = partes[3].split(PARENTESIS1);
						agno = Integer.parseInt(arregloA�os[arregloA�os.length-1].split(PARENTESIS2)[0]);
					}
					catch(Exception e){

					}
					finally{
						temp.setAgnoPublicacion(agno);
						tempAgno.setAgno(agno);
					}
					
					

				}

				misPeliculas.agregarElementoFinal(temp);
				peliculasAgno.agregarElementoFinal(tempAgno);				
				line = br.readLine();

			}
			tempAgno.setPeliculas(misPeliculas);			
			
			System.out.println(peliculasAgno.darNumeroElementos() );
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}



	private void ordenarListaVOPelicual(ILista<VOAgnoPelicula> peliculasAgno2) {
		// TODO Auto-generated method stub
		for (int i = 1; i < peliculasAgno2.darNumeroElementos(); i++) {
			VOAgnoPelicula temp = peliculasAgno2.darElemento(i);
			int j;
			for ( j = i-1; j>=0 && temp.getAgno() <peliculasAgno2.darElemento(j).getAgno(); j--) {
				peliculasAgno2.darElemento(j+1).setAgno(j);
				peliculasAgno2.darElemento(j+1).setAgno(temp.getAgno());
			}
		}
	}



	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
}


