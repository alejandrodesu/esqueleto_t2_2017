package model.data_structures;

public class NodeDoble<T> {
	private NodeDoble<T> next; 
	private NodeDoble<T> prev; 
	private T item; 
	private T top;

	public NodeDoble(T item) 
	{
		next=null;
		this.item=item;
	}

	public NodeDoble<T> getNext() {
		return next;
	}
	public NodeDoble<T> getPrev()
	{
		return prev; 
	}
	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}

	public void setNext(NodeDoble<T> next) {
		this.next = next;
	}
	public void setPrev(NodeDoble<T> prev){
		this.prev = prev; 
	}

	}

