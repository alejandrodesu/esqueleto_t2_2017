package model.data_structures;

import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.omg.CosNaming.NamingContextPackage.NotEmpty;
import org.w3c.dom.ls.LSInput;

	
public class ListaEncadenada<T> implements ILista<T> {

	private Node<T> primero;
	private Node<T> actual;
	private int listSize;
	
	public ListaEncadenada()
	{
		primero = new Node<T>();
		actual = primero;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
<<<<<<< HEAD
		final ListaEncadenada<T> lista = this; 
		return new Iterator<T>() {
			Node<T> nodoActual = null; 
			
			public boolean hasNext()
			{
				if (nodoActual==null)
				{
					return list.getItem() != null; 
				}
				else 
				{
					return nodoActual.getNext() != null; 
				}
=======
		return new Iterator<T>() 
			{
			private Node<T> actualT=null;
	
			public boolean hasNext() 
			{
				if(actualT==null)return primero.getItem()!=null;
				else return actualT.getNext() != null;
>>>>>>> eae0c3bb681eb1b87b496a1261d90c3cba8a9aae
			}
	
			public T next() 
			{
<<<<<<< HEAD
				if (nodoActual==null)
				{
					nodoActual = list; 
					if (nodoActual==null)return null;
					else return nodoActual.getItem(); 
				}
				else 
				{
					nodoActual=nodoActual.getNext(); 
					return nodoActual.getItem();
				}
			}
			@Override
			public void remove() 
			{
				
			}

		};

	}


	private Node<T> list;

	private int size;
=======
				if(actualT==null)
				{
					actualT=primero;
					if(actualT==null)return null;
					else return actualT.getItem();
				}
				else
				{
					actualT = actualT.getNext();
					return actualT.getItem();
				}
>>>>>>> eae0c3bb681eb1b87b496a1261d90c3cba8a9aae
	
			}
		};
}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(primero.getItem() == null)
		{
			primero.setItem((Node<T>) elem);
			return;
		}
		else
		{
			Node<T> act = primero;
			while(act != null)
			{
				if (act.getNext()== null)
				{
					act.setItem(new Node<T>());
					act.getNext().setItem((Node<T>) elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		listSize++;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return (T) actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		Node<T> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		Node<T> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}

}