package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListaDobleEncadenada<T> implements ILista<T> {

	NodeDoble<T> list; 
	private int size; 
	private NodeDoble<T> actual;
	private NodeDoble<T> ultimo;

	public Iterator<T> iterator() 
	{
		final ListaDobleEncadenada<T> lista = this; 
		return new Iterator<T>() {
			final NodeDoble<T> primero = lista.list; 
			NodeDoble<T> nodoActual = null; 

			public boolean hasNext()
			{
				if (lista.emptyList())
				{
					return false; 
				}
				else if (nodoActual==null)
				{
					return false; 
				}
				else if (nodoActual == lista.darUltimaPosicion())
				{
					return false; 
				}
					
				return true; 
			}
			public T next()
			{  
				if (lista.emptyList())
				{
					throw new NoSuchElementException();
				}
				else if (nodoActual == null)
				{
					this.nodoActual = primero; 
					return nodoActual.getItem();
				}
				else if (nodoActual.getNext() == null)
				{
					throw new NoSuchElementException(); 
				}
				this.nodoActual = nodoActual.getNext(); 
				return nodoActual.getItem(); 
			}
			@Override
			public void remove() 
			{

			}

		};

	}

	public ListaDobleEncadenada()
	{
		list = null; 
	}
	public ListaDobleEncadenada(T item)
	{
		list = new NodeDoble<T>(item);
		size=1;
	}


	@Override
	public void agregarElementoFinal(T elem) 
	{
		NodeDoble<T> newNodo = new NodeDoble<T>(elem);
		newNodo.setNext(null);
		if(list==null)
		{
			list = newNodo;
			ultimo = newNodo;
		}
		else
		{
			newNodo.setPrev(ultimo);
			ultimo.setNext(newNodo);
			ultimo = newNodo;
		}
		size++;
		actual = list;
	}

	@Override
	public T darElemento(int pos)
	{
		int aBuscar=0; 
		T buscado = null; 
		if (list==null)
		{
			return null; 
		}
		else if (pos==1)
		{
			return (T)list.getNext(); 
		}
		else if (esUltimo())
		{
			return darUltimaPosicion(); 
		}
		else
		{
			while (list.getNext() != null)
			{
				aBuscar++; 
				if (aBuscar==pos)
				{
					buscado = (T) list.getItem(); 
				}

			}
		}
		// TODO Auto-generated method stub
		return buscado; 
	}
	public boolean esUltimo()
	{
		if (list==null) {
			return false;
		}
		if (list.getNext()==null) {
			return true;
		}
		else {
			return false;
		}

	}
	public T darUltimaPosicion()
	{
		Node <T> ultimo =null;
		if (list==null) {
			return null;
		}
		else {
			while (list.getNext()!=null) {
				if (list.getNext()==null){
					ultimo=(Node<T>) list.getItem();
				}
			}
		}
		return (T) ultimo;
	}


	@Override
	public int darNumeroElementos() 
	{
		int total = 1;
		if (list==null) {
			return 0;
		}else {
			while (list.getNext()!=null) {
				total++;
			}
		}
		return total;
	}

	@Override
	public T darElementoPosicionActual()
	{
		Node<T> iterator = (Node<T>)iterator();
		Node<T> actual = iterator; 
		T ans = actual.getItem(); 
		return ans;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if (list.getNext() != null)
		{
			return true; 
		}
		return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		if(actual!=null&&actual.getPrev()!= null)
		{
			actual = actual.getPrev();
			return true;
		}
		return false;
	}
	public boolean emptyList()
	{
		return list==null; 
	}
	public void deleteLast(){
		NodeDoble<T> aux = list; 
		NodeDoble<T> prev = null; 
		if(aux.getNext() == null )
		{
			emptyList(); 
		}
		if (!emptyList())
		{
			aux = list; 
			while(aux.getNext().getNext() != null)
			{
				aux = aux.getNext(); 
				prev = aux.getPrev(); 
			}
			aux.setNext(null);
			aux.setPrev(prev);
		}

	}
	public void deleteFirst()
	{

		NodeDoble<T> aux; 
		NodeDoble<T> prev; 
		NodeDoble<T> temp; 
		if (!emptyList())
		{
			aux=list; 
			temp = list.getNext(); 
			prev = temp.getPrev(); 
			list = temp; 
			prev=null; 
			aux = null; 
		}
	}
}
