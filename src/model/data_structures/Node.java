package model.data_structures;

public class Node<T> 
{
	private Node<T> next;  
	private T item; 
	
	public Node()
	{
		next=null;
		this.item=null;
	}
	
	public Node<T> getNext() {
		return next;
	}

	public T getItem() {
		return item;
	}
	
	public void setItem(Node<T> node) {
		this.item = (T) node;
	}
	
	public void setNext(Node<T> next) {
		this.next = next;
	}

	
}
