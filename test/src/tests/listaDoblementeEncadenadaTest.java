package tests;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class listaDoblementeEncadenadaTest extends TestCase
{
	private ListaDobleEncadenada<String> lista2;
	private Iterator<String> iterador;
	
	private void setupEscenario1()
	{
		lista2 = new ListaDobleEncadenada<String>();
	}
	
	private void setupEscenario2()
	{
		lista2 = new ListaDobleEncadenada<String>();
		lista2.agregarElementoFinal("Elemento A");
	}
	
	private void setupEscenario3()
	{
		lista2 = new ListaDobleEncadenada<String>();
		lista2.agregarElementoFinal("Elemento A");
		lista2.agregarElementoFinal("Elemento B");
		lista2.agregarElementoFinal("Elemento C");
	}
	
	public void testAgregarElementoFinal()
	{
		setupEscenario1();
		lista2.agregarElementoFinal("Elemento A");
		lista2.agregarElementoFinal("Elemento B");
		lista2.agregarElementoFinal("Elemento C");
		assertEquals(lista2.darElemento(0), "Elemento A");
		assertEquals(lista2.darElemento(1), "Elemento B");
		assertEquals(lista2.darElemento(2), "Elemento C");
	}
	
	public void testDarElemento()
	{
		setupEscenario3();
		assertEquals(lista2.darElemento(0), "Elemento A");
		assertEquals(lista2.darElemento(1), "Elemento B");
		assertEquals(lista2.darElemento(2), "Elemento C");
		
	}
	
	public void testDarNumeroElementos()
	{
		setupEscenario3();
		assertEquals(lista2.darNumeroElementos(), 3);
	}
	
	public void testDarElementoPosicionActual()
	{
		setupEscenario2();
		assertEquals(lista2.darElementoPosicionActual(), "Elemento A");
	}
	
	public void testAvanzar()
	{
		setupEscenario2();
		assertFalse(lista2.avanzarSiguientePosicion());
		lista2.agregarElementoFinal("Elemento B");
		lista2.darElemento(0);
		assertTrue(lista2.avanzarSiguientePosicion());
	}
	
	public void testRetroceder()
	{
		setupEscenario2();
		assertFalse(lista2.retrocederPosicionAnterior());
		
		lista2 = null;
		
		setupEscenario3();
		assertEquals(lista2.darElementoPosicionActual(), "Elemento A");
		lista2.darElemento(2);
		assertTrue(lista2.retrocederPosicionAnterior());
		assertEquals(lista2.darElementoPosicionActual(), "Elemento B");
	}
	
	public void testIterator()
	{
		setupEscenario1();
		iterador = lista2.iterator();
		assertFalse("Deber�a ser falso", iterador.hasNext());
		assertNull("Deber�a ser null", iterador.next());
		setupEscenario3();
		iterador = lista2.iterator();
		assertTrue("Deber�a ser verdadero", iterador.hasNext());
		assertEquals("Elemento A", iterador.next());
	}
}
