package tests;

import java.util.Iterator;

import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase
{
	private ListaEncadenada<String> list1; 
	private Iterator<String>iterator; 
	private void setupEscenario1()
	{
		try {list1 = new ListaEncadenada<>();} 
		catch (Exception e) {fail("No exception expected");}
	}
	private void setupEscenario2()
	{
		try {list1= new ListaEncadenada<>();list1.agregarElementoFinal("Movie A");} 
		catch (Exception e) { fail("No exception expected");}
	}
	private void setupEscenario3()
	{
		try {
			list1 = new ListaEncadenada();
			list1.agregarElementoFinal("Movie A");
			list1.agregarElementoFinal("Movie B");
			list1.agregarElementoFinal("Movie c");
		} catch (Exception e) {fail("No exception expected");}
	}
	private void setupEscenario4(){
		try {
			list1 = new ListaEncadenada<>(); 
			list1.agregarElementoFinal("Movie A");
			list1.agregarElementoFinal("Movie B");
			list1.agregarElementoFinal("Movie C");
			list1.agregarElementoFinal("Movie D");
			list1.agregarElementoFinal("Movie E");
		} catch (Exception e) {fail("No exception expected");}
	}
	public void testIterator()
	{
		setupEscenario2();
		iterator = list1.iterator(); 
		System.out.println(iterator.hasNext());
		setupEscenario3();
		iterator = list1.iterator(); 
		assertTrue("True Expected", iterator.hasNext());
		assertEquals("Movie A expected", "Movie A",iterator.next());
	}
	public void testAgregarElementoFinal()
	{
		setupEscenario1();
		list1.agregarElementoFinal("A");
		list1.agregarElementoFinal("B");
		list1.agregarElementoFinal("C");
		assertEquals("A", list1.darElemento(0));
		assertEquals("B", list1.darElemento(1));
		assertEquals("C", list1.darElemento(2));
	}
	public void testDarElemento()
	{
		setupEscenario3();
		assertEquals("Movie A", list1.darElemento(0));
		assertEquals("Movie B", list1.darElemento(1));
		assertEquals("Movie C", list1.darElemento(2));
		
	}
	public void testDarNumeroElementos()
	{
		setupEscenario4();
		assertEquals(5, list1.darNumeroElementos());
	}
	public void testDarElementoPosicionActual()
	{
		setupEscenario1();
		assertNull("Null Expected", list1.darElementoPosicionActual());
		setupEscenario3();
		assertEquals("Movie C Expected", "Movie C", list1.darElementoPosicionActual());
	}
	public void testRetrocederPosicionAnterior()
	{
		setupEscenario1();
		assertFalse("No debe retroceder", list1.retrocederPosicionAnterior());
		list1.agregarElementoFinal("Movie B");
		assertFalse("No debe retroceder", list1.retrocederPosicionAnterior());
		setupEscenario3();
		assertTrue("Deberia retroceder", list1.retrocederPosicionAnterior());
	}
}
